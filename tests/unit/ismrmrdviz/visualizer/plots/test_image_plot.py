import pytest
import numpy as np
from pyscivis.visualizer.dataclasses.config import ImageConfig
from pyscivis.visualizer.dataclasses.parser import ParsedData
from pyscivis.visualizer.dataclasses.state import State, DimensionState, AxesState, ImageState, \
    ROIState, RangeState, ProfileState, PaletteState
from pyscivis.visualizer.models import ThrottledSlider
from pyscivis.visualizer.plots import ImagePlot

# TODO: WIP

@pytest.fixture
def image_plot(monkeypatch):
    monkeypatch.delattr(ThrottledSlider, "on_change")
    data = ParsedData(
        data=np.arange(2000).reshape(10, 20, 10),
        dim_names=["z", "y", "x"],
        dim_lengths=[10, 20, 20],
        dim_units=["pixel", "mm", "mm"]
    )
    return ImagePlot(data, ImageConfig(palette_size=256))


state_ftf = State(
    dimension=DimensionState(axes=AxesState(x_axis="y", y_axis="x"), dimensions=dict(x=2, y=3, z=1)),
    image=ImageState(
        roi=ROIState(x=[], y=[], width=[], height=[]),
        x_range=RangeState(start=1.0, end=18.0, bounds=(0.0, 18.0)),
        y_range=RangeState(start=0.0, end=9.0, bounds=(0.0, 9.0))
    ),
    profile=ProfileState(x=13, y=7),
    palette=PaletteState(name="Greyscale", length=256, cutoff=(1, 211), window=(2, 123)),
    active_color_mapper=0,
    fit_to_frame=True
    )


@pytest.mark.parametrize("state", [state_ftf])
def test_set_and_get_state(image_plot: ImagePlot, state: State):
    """
    This test is extremely important, as the different 'state-setters'
    have possible side-effects on other 'state-setters' and the order
    in which the states are set is crucial to set the state correctly.
    """
    image_plot.set_state(state)
    assert image_plot.get_state() == state
