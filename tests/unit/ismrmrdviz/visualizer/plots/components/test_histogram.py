from unittest.mock import Mock

import pytest
import numpy as np
from pyscivis.visualizer.plots.components import HistogramFigure


def histogram_too_many_values():
    data = np.arange(1, 20)
    min_ = 0
    max_ = 19
    hist = HistogramFigure(data, min_, max_, max_calc_size=10)
    return hist


def histogram_ok_amount_of_values():
    data = np.arange(1, 10)
    min_ = 0
    max_ = 9
    hist = HistogramFigure(data, min_, max_, max_calc_size=10)
    return hist


def histogram_only_nans():
    data = np.full(10, np.nan)
    min_ = np.nan
    max_ = np.nan
    hist = HistogramFigure(data, min_, max_, max_calc_size=10)
    return hist


def histogram_some_nans():
    data = np.array([0, 1, 2, 3, np.nan, np.nan, 6])
    min_ = 0
    max_ = 6
    hist = HistogramFigure(data, min_, max_, max_calc_size=10)
    return hist


def histogram_only_zeros():
    data = np.full(10, 0)
    min_ = 0
    max_ = 0
    hist = HistogramFigure(data, min_, max_, max_calc_size=10)
    return hist


def test_histogram_too_many_values_init():
    h = histogram_too_many_values()

    errors = []
    if not h.no_display.visible:
        errors.append("no_display-title should be visible")
    if h.h_main.visible:
        errors.append("main histogram should be hidden")
    if h.h_selected.visible:
        errors.append("selected histogram should be hidden")

    assert not errors, "errors occurred:\n{}".format("\n".join(errors))


def test_histogram_ok_amount_of_values_init():
    h = histogram_ok_amount_of_values()

    errors = []
    if h.no_display.visible:
        errors.append("no_display-title should be hidden")
    if not h.h_main.visible:
        errors.append("main histogram should be visible")
    if not h.h_selected.visible:
        errors.append("selected histogram should be visible")

    assert not errors, "errors occurred:\n{}".format("\n".join(errors))


@pytest.mark.parametrize('min_, max_', [
    (0, 0), (0, 1), (np.nan, np.nan), (np.nan, 0)
])
def test_update_min_max(min_, max_):
    h = histogram_too_many_values()  # hist init state does not matter
    h.update_min_max(min_, max_)
    assert h.fig.y_range.start != h.fig.y_range.end


data = np.arange(-1, 1, 0.1)  # [-1., -0.9, ..., 0.9]
@pytest.mark.parametrize('start, end, expected', [
    (-1, 1, [1]*20),
    (-1, 0.9, [1]*20),
    (-1, 0, [1]*11),
    (0, 1, [1]*10),
    (0.15, 0.85, [1]*8),  # partial edges are included (the edges 0.1 and 0.9)
])
def test_filtered_histogram(start, end, expected):
    hist, edges = HistogramFigure.filtered_histogram(start, end, data, bins=20)
    assert list(hist) == expected

def test_rebin_histogram():
    data = np.arange(10)
    min_ = 0
    max_ = 9
    hist = HistogramFigure(data, min_, max_, max_calc_size=10, bins=10, enable_dynamic_rebinning=False)
    hist.fig.x_range.update(start=3, end=6)

    hist.rebin_histogram()
    # went from 0-9 to 3-6 so we would need a total of 9/(6-3)*bins = 3*10=30 bins
    assert hist.cur_bins == 30


@pytest.mark.parametrize('histogram', [
    histogram_too_many_values(),
    histogram_ok_amount_of_values()
])
class TestVisibilityWithDifferentHistograms:

    def test_update_both_too_big(self, histogram: HistogramFigure):
        h = histogram

        errors = []
        all_ = selected = np.arange(11)
        h.update(all_, selected)

        if not h.no_display.visible:
            errors.append("no_display-title should be visible")
        if h.h_main.visible:
            errors.append("main histogram should be hidden")
        if h.h_selected.visible:
            errors.append("selected histogram should be hidden")

        assert not errors, "errors occurred:\n{}".format("\n".join(errors))

    def test_update_selected_ok(self, histogram: HistogramFigure):
        h = histogram

        errors = []
        all_ = np.arange(11)
        selected = np.arange(10)
        h.update(all_, selected)

        if h.no_display.visible:
            errors.append("no_display-title should be hidden")
        if not h.h_selected.visible:
            errors.append("selected histogram should be visible")

        assert not errors, "errors occurred:\n{}".format("\n".join(errors))

    def test_update_both_ok(self, histogram: HistogramFigure):
        h = histogram

        errors = []
        all_ = selected = np.arange(10)
        h.update(all_, selected)

        if h.no_display.visible:
            errors.append("no_display-title should be hidden")
        if not h.h_main.visible:
            errors.append("main histogram should be visible")
        if not h.h_selected.visible:
            errors.append("selected histogram should be visible")

        assert not errors, "errors occurred:\n{}".format("\n".join(errors))

    def test_update_both_ok_but_different(self, histogram: HistogramFigure):
        h = histogram

        errors = []
        all_ = np.arange(10)
        selected = np.arange(6)
        h.update(all_, selected)

        if h.no_display.visible:
            errors.append("no_display-title should be hidden")
        if not h.h_main.visible:
            errors.append("main histogram should be visible")
        if not h.h_selected.visible:
            errors.append("selected histogram should be visible")

        assert not errors, "errors occurred:\n{}".format("\n".join(errors))

    def test_update_selection_with_too_many_values_in_selection(self, histogram: HistogramFigure):
        h = histogram

        errors = []
        h.update_selection(np.arange(11))

        if not h.no_display.visible:
            errors.append("no_display-title should be visible")
        if h.h_selected.visible:
            errors.append("selected histogram should be hidden")

        assert not errors, "errors occurred:\n{}".format("\n".join(errors))

    def test_update_selection_with_ok_amount_of_values_in_selection(self, histogram: HistogramFigure):
        h = histogram

        errors = []
        h.update_selection(np.arange(10))

        if h.no_display.visible:
            errors.append("no_display-title should be hidden")
        if not h.h_selected.visible:
            errors.append("selected histogram should be visible")

        assert not errors, "errors occurred:\n{}".format("\n".join(errors))
