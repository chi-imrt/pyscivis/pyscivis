import math
from typing import Tuple, Optional
from unittest.mock import Mock

import pytest
from bokeh.models import LinearColorMapper
from bokeh.palettes import grey, turbo
from pyscivis.visualizer.dataclasses.state import PaletteState

from pyscivis.visualizer.models import OffsetLogColorMapper
from pyscivis.visualizer.plots.components import PaletteControls

@pytest.fixture
def palette_param(request):
    low = request.param[0]
    high = request.param[1]
    palette_map = dict(Greyscale=[grey(n) for n in range(256)],
                       Turbo=[turbo(n) for n in range(256)])
    palette = palette_map["Greyscale"][-1]
    cms = (LinearColorMapper(palette, low=low, high=high),
           OffsetLogColorMapper(palette, low=low, high=high))
    fig = PaletteControls(cms, palette_map, 256, "Greyscale", min_=low, max_=high)
    return fig


@pytest.mark.parametrize('palette_param', [
    [-1, 5],
    [float("nan"), float("nan")],
    [1, float("nan")],
    [float("nan"), 1],
    [1, 1],
], indirect=True)
def test_init(palette_param: PaletteControls):
    assert palette_param.palette_window.start != palette_param.palette_window.end
    assert not math.isnan(palette_param.palette_window.start)


@pytest.fixture()
def palette_and_cm():
    palette_map = dict(Greyscale=[grey(n+1) for n in range(255)])
    mock_cm = Mock()
    cms = (mock_cm,)
    fig = PaletteControls(cms, palette_map, 256, "Greyscale", min_=0, max_=10)
    return fig, mock_cm


@pytest.mark.parametrize("state", [
    PaletteState("Greyscale", 50, (-10, 30), (-12, 10))
])
def test_set_and_get_state(palette_and_cm: Tuple[PaletteControls, Mock], state: PaletteState):
    palette = palette_and_cm[0]
    palette.set_state(state)
    assert palette.get_state() == state


front_color = "#000000"
back_color = "#ffffff"


@pytest.mark.parametrize("name, length, cutoff, expected", [
    ["Greyscale", 10, (0, 10), list(grey(10))],  # left slider at start, right slider at end
    ["Greyscale", 10, (10, 10), 10*[front_color]],  # sliders at the rightmost position
    ["Greyscale", 10, (0, 0), 10*[back_color]],  # sliders at the leftmost position
    ["Greyscale", 10, (5, 10), 5*[front_color]+list(grey(10)[5:])],  # left slider in the middle, right at end
    ["Greyscale", 10, (0, 5), list(grey(10)[:5])+5*[back_color]],  # left slider at start, right in the middle
    ["Greyscale", 10, (3, 7), 3*[front_color]+list(grey(10)[3:7])+3*[back_color]]  # both sliders +- in the middle
])
def test_palette_cb(palette_and_cm: Tuple[PaletteControls, Mock], name, length, cutoff, expected):
    p = palette_and_cm[0]
    cm = palette_and_cm[1]
    p.palette._callbacks.clear()
    p.palette_length._callbacks.clear()
    p.palette_cutoff._callbacks.clear()

    p.palette.value = name
    p.palette_length.value = length
    p.palette_cutoff.value = cutoff

    p._palette_cb(0, 0, 0)
    assert cm.palette == expected


@pytest.mark.parametrize("low, high", [
    [3, 7]
])
def test_window_cb(palette_and_cm: Tuple[PaletteControls, Mock], low, high):
    p = palette_and_cm[0]
    cm = palette_and_cm[1]

    p._window_cb(0, 0, (low, high))
    cm.update.assert_called_once_with(low=low, high=high)

@pytest.fixture
def palette():
    palette_map = dict(Greyscale=[grey(n+1) for n in range(255)])
    cms = (Mock(),)
    palette = PaletteControls(cms, palette_map, 256, "Greyscale", min_=0, max_=10)
    return palette


@pytest.mark.parametrize("min_, max_, expected", [
    [None, 1, "base"],
    [1, None, "base"],
    [None, None, "base"],
    [float("nan"), 1, "disabled"],
    [1, float("nan"), "disabled"],
    [float("nan"), float("nan"), "disabled"],
    [1, 1, "disabled"],
    [0, 2, "enabled"],
])
def test_reset_window_sliders_(palette: PaletteControls, min_: Optional[float], max_: Optional[float], expected, monkeypatch):
    # monkeypatch
    monkeypatch.setattr(palette, "enable_and_attach_callbacks", Mock())
    monkeypatch.setattr(palette, "disable_and_detach_callbacks", Mock())

    # change current ranges without callbacks
    palette.palette_window._callbacks.clear()
    palette.palette_cutoff._callbacks.clear()
    palette.palette_window.value = (1, 9)
    palette.palette_cutoff.value = (1, 9)
    # actual function call
    palette.reset_window_sliders(min_, max_)

    # asserts
    if expected == "disabled":
        palette.disable_and_detach_callbacks.assert_called_once()
        assert palette.palette_window.value == (1, 9) and palette.palette_cutoff.value == (1, 9)
    elif expected == "enabled":
        palette.enable_and_attach_callbacks.assert_called_once()
        assert palette.palette_window.value == (min_, max_) and palette.palette_cutoff.value == (min_, max_)
    elif expected == "base":
        assert palette.palette_window.value == (0, 10) and palette.palette_cutoff.value == (0, 10)


def test_enable_and_attach_callbacks(palette):
    palette.enable_and_attach_callbacks()
    assert not palette.palette_window.disabled and not palette.palette_cutoff.disabled
    assert palette.palette_window._callbacks and palette.palette_window._callbacks


def test_disable_and_detach_callbacks(palette):
    palette.disable_and_detach_callbacks()
    assert palette.palette_window.disabled and palette.palette_cutoff.disabled
    assert not palette.palette_window._callbacks and not palette.palette_window._callbacks