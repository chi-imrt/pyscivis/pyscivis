import pytest
from pyscivis.visualizer.plots.components.crosshair import Crosshair


@pytest.fixture()
def crosshair():
    return Crosshair()


def test_set_coordinates(crosshair):
    crosshair.set_coordinates(10, 5)
    assert crosshair.x_line.location == 5
    assert crosshair.y_line.location == 10


@pytest.mark.dependency()
def test_toggle_visibility_with_false(crosshair):
    crosshair.toggle_visible(False)
    assert not crosshair.x_line.visible and not crosshair.y_line.visible


@pytest.mark.dependency(depends=["test_toggle_visibility_with_false"])
def test_toggle_visibility_with_true(crosshair):
    crosshair.toggle_visible(False)
    crosshair.toggle_visible(True)
    assert crosshair.x_line.visible and crosshair.y_line.visible


@pytest.mark.dependency()
def test_toggle_visibility_with_none(crosshair):
    crosshair.toggle_visible(None)
    assert not crosshair.x_line.visible and not crosshair.y_line.visible


@pytest.mark.dependency(depends=["test_toggle_visibility_with_none"])
def test_toggle_visibility_with_none_two(crosshair):
    crosshair.toggle_visible(None)
    crosshair.toggle_visible(None)
    assert crosshair.x_line.visible and crosshair.y_line.visible
