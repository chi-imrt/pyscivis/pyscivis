from typing import Union
from unittest.mock import Mock

import pytest
from pyscivis.visualizer.plots.components.value_controls import ValueRadios, LABEL_FUNC_MAP


@pytest.fixture()
def value_radios():
    init_func_name = list(LABEL_FUNC_MAP.keys())[0]
    return ValueRadios(Mock(), init_func_name)


def test_init(value_radios: ValueRadios):
    assert value_radios.active_handler is list(LABEL_FUNC_MAP.values())[0]


@pytest.mark.parametrize("new_index", [0, 1, 2, 3])
def test_notify_parent(value_radios: ValueRadios, new_index):
    value_radios.rg.active = new_index

    assert value_radios.active_handler is list(LABEL_FUNC_MAP.values())[new_index]


@pytest.mark.parametrize("new_index", list(LABEL_FUNC_MAP.keys()))
def test_set_active_handler_with_str(value_radios: ValueRadios, new_index: Union[str, int]):
    value_radios.set_active_handler(new_index)
    assert list(LABEL_FUNC_MAP)[value_radios.rg.active] == new_index


@pytest.mark.parametrize("new_index", list(range(len(LABEL_FUNC_MAP))))
def test_set_active_handler_with_int(value_radios: ValueRadios, new_index: Union[str, int]):
    value_radios.set_active_handler(new_index)
    assert value_radios.rg.active == new_index


@pytest.mark.parametrize("value_kind", [i+1 for i in range(len(LABEL_FUNC_MAP)-1)])
def test_set_and_get_state(value_radios: ValueRadios, value_kind: int, monkeypatch):
    monkeypatch.setattr(value_radios, "notify_parent", Mock())
    value_radios.set_state(value_kind)
    assert value_radios.get_state() == value_kind
    value_radios.notify_parent.assert_called_once()
