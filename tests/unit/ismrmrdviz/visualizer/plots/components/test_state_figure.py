import base64
from unittest.mock import Mock

import pytest
from pyscivis.visualizer.dataclasses.state import state_from_json, state_to_json
from pyscivis.visualizer.plots.components.state import StateFigure


valid_json_state = """
    {"dimension":
         {"axes":
              {"x_axis": "X",
               "y_axis": "Y"},
          "dimensions": {"REPETITION": 1, "COIL": 1, "Y": 1, "X": 1}
          },
     "image": {"roi": {"x": [], "height": [], "y": [], "width": []},
               "y_range": {"end": 128.0, "bounds": [0.0, 128.0], "start": 0.0},
               "x_range": {"end": 256.0, "bounds": [0.0, 256.0], "start": 0.0}},
     "palette": {"length": 100, "window": [-0.00466897850856185, 0.0039265574887394905],
                 "name": "Greyscale", "cutoff": [-0.00466897850856185, 0.0039265574887394905]}, "profile": {"x": null, "y": null},
     "active_color_mapper": 0, "fit_to_frame": false, "value_kind": 0}
 """

valid_state_b64 = base64.b64encode(valid_json_state.encode())

valid_state = state_from_json(valid_json_state)

STATE_TEXT_BOX_IDX = 0
STATE_TEXT_BTN_ROW_IDX = 1
STATE_FILE_BTN_ROW_IDX = 2
TEXT_GET_BTN_IDX = 0
TEXT_SET_BTN_IDX = 1
FILE_DOWNLOAD_BTN_IDX = 0
FILE_UPLOAD_BTN_IDX = 1
FILE_HIDDEN_UPLOAD_BTN_IDX = 2

@pytest.fixture()
def state_and_cbs():
    set_mock = Mock()
    get_mock = Mock(return_value=valid_state)
    return StateFigure(False, set_mock, get_mock), get_mock, set_mock


def test_testbox_validation(state_and_cbs):
    state, *_ = state_and_cbs
    set_btn = state.children[STATE_TEXT_BTN_ROW_IDX].children[TEXT_SET_BTN_IDX]
    dl_btn = state.children[STATE_FILE_BTN_ROW_IDX].children[FILE_DOWNLOAD_BTN_IDX]

    state.children[STATE_TEXT_BOX_IDX].value = valid_json_state
    assert not set_btn.disabled and not dl_btn.disabled

    state.children[STATE_TEXT_BOX_IDX].value = "invalid json[]"
    assert set_btn.disabled and dl_btn.disabled


def test_set_click(state_and_cbs):
    state, _, set_cb = state_and_cbs
    set_btn = state.children[STATE_TEXT_BTN_ROW_IDX].children[TEXT_SET_BTN_IDX]
    state.children[STATE_TEXT_BOX_IDX].value = valid_json_state
    set_btn_on_click = set_btn._event_callbacks['button_click'][0]
    set_btn_on_click("event")

    set_cb.assert_called_once()


def test_uploading_b64(state_and_cbs):
    state, *_ = state_and_cbs
    text_box = state.children[STATE_TEXT_BOX_IDX]
    hidden_file_btn = state.children[STATE_FILE_BTN_ROW_IDX].children[FILE_HIDDEN_UPLOAD_BTN_IDX]
    hidden_file_btn_on_change = hidden_file_btn._callbacks["value"][0]
    hidden_file_btn_on_change("value", "",  valid_state_b64)

    assert text_box.value == valid_json_state

def test_get_click(state_and_cbs):
    state, get_cb, _ = state_and_cbs
    text_box = state.children[STATE_TEXT_BOX_IDX]
    get_btn = state.children[STATE_TEXT_BTN_ROW_IDX].children[TEXT_GET_BTN_IDX]
    get_btn_on_click = get_btn._event_callbacks['button_click'][0]
    get_btn_on_click("event")
    get_cb.assert_called_once()

    assert text_box.value == state_to_json(valid_state)
