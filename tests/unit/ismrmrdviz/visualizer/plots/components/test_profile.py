import pytest
from pyscivis.visualizer.plots.components import ProfileFigure

# TODO: WIP

@pytest.mark.parametrize("start, end, factor, expected", [
    (0, 1, 0.1, (-0.1, 1.1)),  # standard input
    (0, 1, 1, (-1, 2)),
    (5, 15, 0.1, (4, 16)),
    (-1, 1, 0.1, (-1.2, 1.2)),
    (1, 1, 0.1, (0.9, 1.1)),  # start equals end
    (1, 1, 0.0, (0.9, 1.1)),
    (1, 1, 0.3, (0.7, 1.3)),
    (0, float("nan"), 0.1, (0, 1)),  # NaNs in input
    (float("nan"), 0, 0.1, (0, 1)),
    (float("nan"), float("nan"), 0.1, (0, 1))
])
def test_pad_bounds(start, end, factor, expected):
    assert ProfileFigure.pad_bounds(start, end, factor) == expected


def test_pad_bounds_invalid_arg():
    with pytest.raises(ValueError):
        ProfileFigure.pad_bounds(1, 0)
