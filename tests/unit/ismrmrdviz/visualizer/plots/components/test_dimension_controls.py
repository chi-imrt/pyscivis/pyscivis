from unittest.mock import Mock, call

import pytest
from pyscivis.visualizer.dataclasses.handler import Dim
from pyscivis.visualizer.dataclasses.state import DimensionState, AxesState
from pyscivis.visualizer.models import ThrottledSlider
from pyscivis.visualizer.plots.components.dimension_controls import DimensionSelectPair, DimensionSlider, \
    DimensionControls


class TestDimensionControls:
    @pytest.fixture()
    def dc(self, monkeypatch):
        # we want to use the unmodified on_change because it works without
        # having a server running
        monkeypatch.delattr(ThrottledSlider, "on_change")

        options = {
            0: Dim("dim0", 5, 0, "None"),
            1: Dim("dim1", 2, 0, "None"),
            2: Dim("dim2", 5, 0, "None"),
            3: Dim("dim3", 2, 0, "None"),
            4: Dim("dim4", 4, 0, "None"),
            5: Dim("dim5", 6, 0, "None"),
            6: Dim("dim6", 9, 0, "None"),
            7: Dim("dim7", 1, 0, "None"),
        }
        return DimensionControls(options, Mock(), Mock())

    def test_init(self, dc):
        assert len(dc.dim_sliders) == 8

        assert dc.dim_sliders[0].layout.visible and\
               dc.dim_sliders[1].layout.visible and\
               dc.dim_sliders[2].layout.visible and\
               dc.dim_sliders[3].layout.visible and\
               dc.dim_sliders[4].layout.visible and\
               dc.dim_sliders[5].layout.visible and\
               not dc.dim_sliders[6].layout.visible and\
               dc.dim_sliders[7] is None

    def test_set_state(self, dc):
        dim_state = dict(
            dim0=5,  # OK, set at end value
            dim1=3,  # MEH, set one over end value
                     # Not setting dim2 on purpose
            dim3=0,  # MEH, set one under start value
            dim4=-12,  # MEH, set under start value
            dim5=4,  # OK, set somewhere in middle
            dim6=1,  # OK, set to start
            dim7=1123123,  # OK, shouldn't matter as None
        )
        state = DimensionState(axes=AxesState("dim0", "dim6"),
                               dimensions=dim_state)

        dim2_val = dc.dim_sliders[2].slider.value
        dc.set_state(state)

        assert dc.dim_sliders[0].slider.value == dc.dim_sliders[0].slider.end and\
               dc.dim_sliders[1].slider.value == dc.dim_sliders[1].slider.end and\
               dc.dim_sliders[2].slider.value == dim2_val and\
               dc.dim_sliders[3].slider.value == dc.dim_sliders[3].slider.start and\
               dc.dim_sliders[4].slider.value == dc.dim_sliders[4].slider.start and\
               dc.dim_sliders[5].slider.value == 4 and\
               dc.dim_sliders[6].slider.value == 1 and\
               dc.dim_sliders[7] is None

        # testing that the axes change correctly is done in a test
        # further down, here we just want to make sure that the change
        # is propagated
        dc.notify_axes_change.assert_called_once_with(0, 6)

    def test_get_state(self, dc):
        # just mixing the values up a bit to avoid accidental assertion
        dc.dim_sliders[0].slider.value = dc.dim_sliders[0].slider.end
        dc.dim_sliders[1].slider.value = dc.dim_sliders[1].slider.start
        dc.dim_sliders[5].slider.value = 3
        dc.dim_sliders[6].slider.value = 7
        dc.axis_select.x_select.value = "dim3"
        dc.axis_select.y_select.value = "dim5"

        new_state = dc.get_state()
        dims = new_state.dimensions
        axes = new_state.axes

        assert dims["dim0"] == dc.dim_sliders[0].slider.value and\
               dims["dim1"] == dc.dim_sliders[1].slider.value and\
               dims["dim2"] == dc.dim_sliders[2].slider.value and\
               dims["dim3"] == dc.dim_sliders[3].slider.value and\
               dims["dim4"] == dc.dim_sliders[4].slider.value and\
               dims["dim5"] == dc.dim_sliders[5].slider.value and\
               dims["dim6"] == dc.dim_sliders[6].slider.value and\
               "dim7" not in dims and\
               axes.x_axis == dc.axis_select.x_select.value and\
               axes.y_axis == dc.axis_select.y_select.value

    @pytest.mark.parametrize("x_axis, y_axis", [
        (0, 1),
        (3, 4),
        (7, 6)
    ])
    def test_on_axes_change(self, dc, x_axis, y_axis):
        dc.disable_active_slider = Mock()
        dc.on_axes_change(x_axis, y_axis)

        dc.disable_active_slider.assert_called_once()
        if dc.dim_sliders[x_axis] is not None:
            assert not dc.dim_sliders[x_axis].layout.visible
        if dc.dim_sliders[y_axis] is not None:
            assert not dc.dim_sliders[y_axis].layout.visible
        for slider in dc.dim_sliders:
            if slider is not None and slider.id not in [x_axis, y_axis]:
                assert slider.layout.visible
        dc.notify_axes_change.assert_called_once_with(x_axis, y_axis)

    @pytest.mark.parametrize("slider_ids", [
        (0, 1, 2, 3, 4, 5, 6, 7)
    ])
    def hide_sliders(self, dc, slider_ids):
        dc.hide_sliders(slider_ids)
        for slider_id in slider_ids:
            slider = dc.dim_sliders[slider_id]
            if slider is not None:
                assert not slider.layout.visible

    @pytest.fixture()
    def dc_mocked_curdoc(self, monkeypatch):
        curdoc_mock = Mock()
        monkeypatch.setattr("pyscivis.visualizer.plots.components.dimension_controls.curdoc", curdoc_mock)

        options = {
            0: Dim("dim0", 5, 0, "None"),
            1: Dim("dim1", 2, 0, "None"),
            2: Dim("dim2", 5, 0, "None"),
        }
        return DimensionControls(options, Mock(), Mock()), curdoc_mock

    @pytest.mark.dependency(name="toggle_works")
    def test_toggle_slider_playback_initial(self, dc_mocked_curdoc):
        dc, curdoc_mock = dc_mocked_curdoc
        temp = dc.disable_active_slider
        dc.disable_active_slider = Mock()

        dc.toggle_slider(0)

        dc.disable_active_slider.assert_not_called()
        dc.disable_active_slider = temp
        curdoc_mock().add_periodic_callback.assert_called_once()
        assert dc.dim_sliders[0].play_btn.label == '❚❚' and\
               dc.animated_dimension == 0

    @pytest.mark.dependency(depends=["toggle_works"])
    def test_toggle_slider_playback_same_btn(self, dc_mocked_curdoc):
        self.test_toggle_slider_playback_initial(dc_mocked_curdoc)

        dc, curdoc_mock = dc_mocked_curdoc
        dc.toggle_slider(0)

        curdoc_mock().remove_periodic_callback.assert_called_once()

    @pytest.mark.dependency(depends=["toggle_works"])
    def test_toggle_slider_playback_other_btn(self, dc_mocked_curdoc):
        self.test_toggle_slider_playback_initial(dc_mocked_curdoc)

        dc, curdoc_mock = dc_mocked_curdoc
        dc.disable_active_slider = Mock()

        dc.toggle_slider(1)

        dc.disable_active_slider.assert_called_once()
        assert dc.dim_sliders[1].play_btn.label == '❚❚' and\
               dc.animated_dimension == 1

    def test_disable_active_slider_no_playback(self, dc_mocked_curdoc):
        dc, curdoc_mock = dc_mocked_curdoc

        dc.disable_active_slider()

        curdoc_mock().remove_periodic_callback.assert_not_called()

    @pytest.mark.dependency(depends=["toggle_works"])
    def test_disable_active_slider_with_playback(self, dc_mocked_curdoc):
        self.test_toggle_slider_playback_initial(dc_mocked_curdoc)
        dc, curdoc_mock = dc_mocked_curdoc

        dc.disable_active_slider()

        assert dc.dim_sliders[0].play_btn.label == '►' and\
               dc.animated_dimension is None
        curdoc_mock().remove_periodic_callback.assert_called_once()


class TestDimensionSlider:
    @pytest.fixture()
    def dsl_and_mock(self, monkeypatch):
        # we want to use the unmodified on_change because it works without
        # having a server running
        monkeypatch.delattr(ThrottledSlider, "on_change")
        mock = Mock()
        return DimensionSlider(10, "name", 20, mock), mock

    def test_play_btn_event(self, dsl_and_mock):
        dsl, mock = dsl_and_mock
        emulate_play_btn_click = dsl.play_btn._event_callbacks["button_click"][0]
        emulate_play_btn_click()

        mock.toggle_slider.assert_called_once_with(dsl.id)

    @pytest.mark.parametrize("n_incs", [
        0, 19, 20
    ])
    def test_inc_slider(self, dsl_and_mock, n_incs):
        dsl, mock = dsl_and_mock
        for _ in range(n_incs):
            dsl.inc_slider()

        assert dsl.slider.value == n_incs % dsl.length
        mock.notify_dim_change.assert_has_calls([call(dsl.id, val % dsl.length) for val in range(1, dsl.slider.value+1)])

    @pytest.mark.parametrize("n_decs", [
        0, 1, 20,
    ])
    def test_dec_slider(self, dsl_and_mock, n_decs):
        dsl, mock = dsl_and_mock
        for _ in range(n_decs):
            dsl.dec_slider()

        assert dsl.slider.value == (dsl.length-n_decs) % dsl.length
        mock.notify_dim_change.assert_has_calls([call(dsl.id, val) for val in range(n_decs-1, 0, -1)])

    def test_show_and_hide(self, dsl_and_mock):
        dsl, _ = dsl_and_mock
        dsl.hide()
        assert not dsl.layout.visible
        dsl.show()
        assert dsl.layout.visible



class TestDimensionSelectPair:
    options_0 = {
        0: Dim("dim0", 5, 0, "None"),
        1: Dim("dim1", 2, 0, "None"),
        2: Dim("dim2", 1, 0, "None"),
    }
    expected_0 = {
        "n_valid_opts": 2,
        "active_x": "dim1",
        "active_y": "dim0",
    }

    @pytest.mark.parametrize("options, expected", [
        [options_0, expected_0],
    ])
    def test_init(self, options, expected):
        dsp = DimensionSelectPair(options, Mock())

        assert len(dsp.options) == expected['n_valid_opts'] and\
               dsp.x_select.value == expected['active_x'] and\
               dsp.y_select.value == expected['active_y']

    @pytest.fixture()
    def dsp(self):
        options = {
            0: Dim("dim0", 2, 0, "None"),
            1: Dim("dim1", 3, 0, "None"),  # current y-axis
            2: Dim("dim2", 4, 0, "None"),  # current x-axis
        }
        return DimensionSelectPair(options, Mock())

    @pytest.mark.parametrize("new_x_str, expected_x_y", [
        ("dim0", [0, 1]),
        ("dim1", [1, 2]),  # toggle
    ])
    def test_on_x_change(self, dsp, new_x_str, expected_x_y):
        old_x_str = dsp.x_select.value
        dsp.on_x_change("whatever", old_x_str, new_x_str)

        dsp.controller.on_axes_change.assert_called_once_with(*expected_x_y)


    @pytest.mark.parametrize("new_y_str, expected_x_y", [
        ("dim0", [2, 0]),
        ("dim2", [1, 2]),  # toggle
    ])
    def test_on_y_change(self, dsp, new_y_str, expected_x_y):
        old_y_str = dsp.y_select.value
        dsp.on_y_change("whatever", old_y_str, new_y_str)

        dsp.controller.on_axes_change.assert_called_once_with(*expected_x_y)