from unittest.mock import Mock

import pytest
import numpy as np

from pyscivis.visualizer.plots.components import StatisticsFigure


@pytest.fixture()
def statistics():
    data = np.ones((10, 10, 10))
    metadata_mock = Mock()
    metadata_mock.x.unit = "km"
    metadata_mock.y.unit = "km"
    metadata_mock.x.length = 100
    metadata_mock.y.length = 100
    metadata_mock.x.size = 1000
    metadata_mock.y.size = 1000

    return StatisticsFigure(data, metadata_mock, max_calc_size=1000)


def test_get_statistics(statistics: StatisticsFigure):
    data = np.ones((10, 10, 10))
    stats = statistics._get_stats_from_arr(data)
    assert stats['mean'] == 1


def test_get_statistics_with_nans(statistics: StatisticsFigure):
    data = np.ones((10, 10, 10)).astype("float")
    data[::2] = np.nan
    stats = statistics._get_stats_from_arr(data)
    assert stats['mean'] == 1


def test_get_statistics_only_nans(statistics: StatisticsFigure):
    data = np.full((10, 10, 10), np.nan)
    stats = statistics._get_stats_from_arr(data)
    assert stats['mean'] == "No data"


def test_get_statistics_empty_array(statistics: StatisticsFigure):
    data = np.array([[]])
    stats = statistics._get_stats_from_arr(data)
    assert stats['mean'] == "No data"


def test_get_statistics_over_threshold(statistics: StatisticsFigure):
    data = np.full((10, 10, 10), 1)
    statistics._max_calc_size = 0
    stats = statistics._get_stats_from_arr(data)
    assert stats['mean'] == "2ManyVals"
