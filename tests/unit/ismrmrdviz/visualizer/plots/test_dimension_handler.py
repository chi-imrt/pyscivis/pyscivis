import pytest
import numpy as np
from attr import asdict
from pyscivis.visualizer.dataclasses.parser import ParsedData
from pyscivis.visualizer.plots import DimensionHandler


@pytest.mark.parametrize("data_shape, names, lengths, units", [
                             (tuple([1, 2]), ["x", "y"], [5, 3], ["cm", "mm"]),
                             (tuple([1, 2, 3]), ["x", "y", "z"], [5, 3, 3], ["cm", "mm", "mm"]),
                             (tuple([1, 2, 3]), ["dim1", "dim2", "dim3"], [1, 2, 3], [None, "cm", "mm"]),
                             (tuple([1, 2, 3, 1, 1, 1, 1]), ["dim1", "dim2", "dim3", "dim4", "dim5", "dim6", "dim7"], [1, 2, 3, 1, 1, 1, 1], [None, "cm", "mm", 1, 1, 1, 1]),
                         ])
def test_dimension_handler_creation_with_valid_inputs(data_shape, names, lengths, units):
    data = np.random.rand(*data_shape)
    parsed = ParsedData(data, names, lengths, units)
    DimensionHandler(parsed)


@pytest.mark.parametrize("data_shape, names, lengths, units", [
                             (tuple([0]), [], [], []),  # empty input
                             (tuple([1]), ["x"], [2], ["mm"]),  # 1-dimensional
                             (tuple([1, 1]), ["x", "x"], [1, 1], ["mm", "mm"]),  # names not unique
                             (tuple([1]), ["x", "y", "z"], [1, 2, 3], ["cm", "mm", "mm"]),  # dims not equal
                             (tuple([1, 2, 3]), ["z"], [1, 2, 3], ["cm", "mm", "mm"]),  # dims not equal
                             (tuple([1, 2, 3]), ["x", "y", "z"], [3], ["cm", "mm", "mm"]),  # dims not equal
                             (tuple([1, 2, 3]), ["x", "y", "z"], [1, 2, 3], ["mm"]),  # dims not equal
                         ])
def test_dimension_handler_creation_with_invalid_inputs(data_shape, names, lengths, units):
    with pytest.raises(ValueError):
        data = np.random.rand(*data_shape)
        parsed = ParsedData(data, names, lengths, units)
        DimensionHandler(parsed)


@pytest.fixture
def dimension_handler():
    data = np.arange(3*3*3).reshape(3, 3, 3)
    # [[[0,1,2],   [3,4,5],   [6,7,8]   ],
    #  [[9,10,11], [12,13,14],[15,16,17]],
    #  [[18,19,20],[21,22,23],[24,25,26]]]
    names = ["z", "y", "x"]
    lengths = [1, 2, 3]
    units = ["a", "b", "c"]
    parsed = ParsedData(data, names, lengths, units)
    return DimensionHandler(parsed, preload_bounds=False)


def test_set_dim_value(dimension_handler):
    dimension_handler.set_dim_value(0, 2)
    assert dimension_handler._values[0] == 2


@pytest.mark.dependency()
def test_get_ordered_values(dimension_handler):
    dimension_handler._values = np.array([50, 100, 150])
    dimension_handler._order = np.array([2, 0, 1])
    assert dimension_handler.get_ordered_values().tolist() == [150, 50, 100]


@pytest.mark.dependency(depends=["test_get_ordered_values"])
def test_get_data(dimension_handler):
    data = dimension_handler.get_data().tolist()
    assert data == [[0, 1, 2], [3, 4, 5], [6, 7, 8]]

    dimension_handler._values[0] = 1
    data = dimension_handler.get_data().tolist()
    assert data == [[9, 10, 11], [12, 13, 14], [15, 16, 17]]


@pytest.mark.parametrize("x_axis, y_axis", [(0, 1), (1, 0), (0, 2), (2, 0), (1, 2), (2, 1)])
def test_change_axes_with_valid_input(dimension_handler, x_axis, y_axis):
    old_order = list(dimension_handler._order)
    dimension_handler.change_axes(x_axis, y_axis)
    assert dimension_handler._order[-2:].tolist() == [y_axis, x_axis]
    if old_order == dimension_handler._order.tolist():
        assert np.all(dimension_handler._cur_data == dimension_handler._orig_data)
    else:
        assert np.any(dimension_handler._cur_data != dimension_handler._orig_data)


@pytest.mark.parametrize("x_axis, y_axis", [(0, 0), (1, 1), (2, 2)])
def test_change_axes_with_invalid_input(dimension_handler, x_axis, y_axis):
    with pytest.raises(ValueError):
        dimension_handler.change_axes(x_axis, y_axis)


def test_get_metadata(dimension_handler):
    meta_data = dimension_handler.get_metadata()
    assert tuple(asdict(meta_data.x).values()) == ('x', 3, 3, "c")
    assert tuple(asdict(meta_data.y).values()) == ('y', 3, 2, "b")

    dimension_handler._order[-2:] = [2, 0]
    meta_data = dimension_handler.get_metadata()
    assert tuple(asdict(meta_data.x).values()) == ('z', 3, 1, "a")
    assert tuple(asdict(meta_data.y).values()) == ('x', 3, 3, "c")


def test_get_data_min_max(dimension_handler):
    _min, _max = dimension_handler.get_data_min_max()
    assert _min == 0
    assert _max == 26


def test_get_local_min_max(dimension_handler):
    _min, _max = dimension_handler.get_local_min_max()
    assert _min == 0
    assert _max == 8


@pytest.mark.parametrize("dim_id, point, expected", [
    #  if {x or y}_id == dim_id -> value 0 will be used for remaining dim (from dim_h init)
    (0, dict(x_id=0, x_coord=-4, y_id=1, y_coord=0), [0, 9, 18]),  # x=0(!), y=0
    (0, dict(x_id=1, x_coord=1, y_id=2, y_coord=1), [4, 13, 22]),  # x=1, y=1
    (0, dict(x_id=2, x_coord=2, y_id=0, y_coord=-4), [2, 11, 20]),  # x=2, y=0(!)
    (1, dict(x_id=0, x_coord=0, y_id=1, y_coord=-4), [0, 3, 6]),  # x=0(!), z=0
    (1, dict(x_id=1, x_coord=-4, y_id=2, y_coord=1), [1, 4, 7]),  # x=1, z=0(!)
    (1, dict(x_id=2, x_coord=2, y_id=0, y_coord=2), [20, 23, 26]),  # x=2, z=2
    (2, dict(x_id=0, x_coord=0, y_id=1, y_coord=0), [0, 1, 2]),  # z=0, y=0
    (2, dict(x_id=1, x_coord=1, y_id=2, y_coord=-4), [3, 4, 5]),  # z=0(!), y=1
    (2, dict(x_id=2, x_coord=-4, y_id=0, y_coord=2), [18, 19, 20]),  # z=2, y=0
])
def test_get_profile(dimension_handler, dim_id, point, expected):
    assert np.all(dimension_handler.get_profile(dim_id, point) == expected)