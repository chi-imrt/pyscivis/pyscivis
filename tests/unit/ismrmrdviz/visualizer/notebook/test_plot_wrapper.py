from typing import Sequence
from unittest.mock import Mock

import pytest
import numpy as np

from pyscivis.visualizer.dataclasses.config import ImageConfig
from pyscivis.visualizer.dataclasses.parser import ParsedData
from pyscivis.visualizer.models import ThrottledSlider
from pyscivis.visualizer.notebook import PlotWrapper, ImagePlot, Optional
from pyscivis.visualizer.plots import ComplexPlot
from pyscivis.visualizer.plots.components.value_controls import LABEL_FUNC_MAP

# Note: We test the non-threadsafe functions, because it makes testing easier
#       The threadsafe functions are only wrapped by an easy decorator anyway.

class TestPlotWrapper:
    @pytest.fixture(autouse=True)
    def _init_self(self, monkeypatch):
        # we want to use the inherited on_change, not the overridden one
        monkeypatch.delattr(ThrottledSlider, "on_change")

        data = np.arange(100).reshape(10, 10).view(dtype=np.complex64)
        parsed = ParsedData(data, ["y", "x"], [10, 5], ["px", "px"])
        plot = ComplexPlot(parsed, config=ImageConfig(initial_value_kind="Real"))
        self.plot_wrapper = PlotWrapper(plot)

    def test_set_state(self):
        ss_mock = self.plot_wrapper.plot.set_state = Mock()
        self.plot_wrapper._set_state("test")

        ss_mock.assert_called_once_with("test")

    def test_get_state(self):
        gs_mock = self.plot_wrapper.plot.get_state = Mock(return_value="test")

        assert self.plot_wrapper.get_state() == "test"
        gs_mock.assert_called_once()

    @pytest.mark.parametrize("name, value, slider_index, expected", [
        ("y", 9, 0, 9),
        ("y", 111, 0, 9),
        ("y", -1, 0, 0),
        ("x", 4, 1, 4)
    ])
    def test_set_dimension(self, name: str, value: int, slider_index: int, expected: int):
        self.plot_wrapper._set_dimension(name, value)
        assert self.plot_wrapper.plot.dim_controls.dim_sliders[slider_index].slider.value == expected

    @pytest.mark.parametrize("name, expected_x, expected_y", [
        ("y", "y", "x"),
        ("x", "x", "y")
    ])
    def test_set_x_axis(self, name: str, expected_x: str, expected_y: str):
        self.plot_wrapper._set_x_axis(name)
        assert self.plot_wrapper.plot.dim_controls.axis_select.x_select.value == expected_x and\
               self.plot_wrapper.plot.dim_controls.axis_select.y_select.value == expected_y

    @pytest.mark.parametrize("name, expected_x, expected_y", [
        ("x", "y", "x"),
        ("y", "x", "y")
    ])
    def test_set_y_axis(self, name: str, expected_x: str, expected_y: str):
        self.plot_wrapper._set_y_axis(name)
        assert self.plot_wrapper.plot.dim_controls.axis_select.x_select.value == expected_x and\
               self.plot_wrapper.plot.dim_controls.axis_select.y_select.value == expected_y

    @pytest.mark.parametrize("name, expected", [
        ("x", True),
        ("y", True),
        ("X", False),
        ("ASD", False),
        ("", False),
        (" ", False),
    ])
    def test_is_valid_dimension_name(self, name: str, expected: bool):
        assert self.plot_wrapper._is_valid_dimension_name(name) == expected

    @pytest.mark.parametrize("name, expected", [
        ("Real", "Real"),
        ("Phase", "Phase"),
        ("Imaginary", "Imaginary"),
        ("Abs", "Abs"),
        ("INVALID", "Real"),
    ])
    def test_set_value_kind(self, name: str, expected: str):
        self.plot_wrapper.plot.value_controls.on_value_change = lambda: None
        self.plot_wrapper._set_value_kind(name)
        assert self.plot_wrapper.plot.value_controls.active_handler == LABEL_FUNC_MAP[expected]

    @pytest.mark.parametrize("name, expected", [
        ("Real", "Real"),
        ("Phase", "Real"),
        ("Imaginary", "Real"),
        ("Abs", "Real"),
        ("INVALID", "Real"),
    ])
    def test_set_value_kind_noncomplex(self, name: str, expected: str):
        data = np.arange(100).reshape(10, 10)
        parsed = ParsedData(data, ["y", "x"], [10, 10], ["px", "px"])
        plot = ImagePlot(parsed, config=ImageConfig(initial_value_kind="Real"))
        self.plot_wrapper = PlotWrapper(plot)

        self.plot_wrapper._set_value_kind(name)
        # imageplot doesnt have value-controls so we use the dim-handler instead
        assert self.plot_wrapper.plot.handler.value_handler == LABEL_FUNC_MAP[expected]


    def test_get_stats(self):
        # we test statistics calculation itself in test_statistics.py
        # so we just want to make sure that we don't get an error
        self.plot_wrapper.get_stats()
        assert True

    @pytest.mark.parametrize("left, bottom, right, expected", [
        (None, None, None, (True, True, True)),
        (None, None, False, (True, True, False)),
        (None, False, False, (True, False, False)),
        (False, False, False, (False, False, False)),
        (True, True, True, (True, True, True)),
    ])
    def test_toggle_controls(self,
                             left,
                             bottom,
                             right,
                             expected
                             ):
        # replace document with a layoutDOM because non-notebook usage does not set the document properly
        # the layoutDOM has the same effect (also offers the select-one-api)
        layout = self.plot_wrapper.plot.document = self.plot_wrapper.plot.get_layout()

        self.plot_wrapper._toggle_controls(left, bottom, right)

        layout.select_one({"name": "col_left"}).visible = expected[0]
        layout.select_one({"name": "col_bottom"}).visible = expected[1]
        layout.select_one({"name": "col_right"}).visible = expected[2]


    # Note: toggle is inactive by default
    @pytest.mark.parametrize("active, expected", [
        (None, True),
        ([None, None], False),
        (False, False),
        (True, True),
    ])
    def test_toggle_fit_to_frame(self, active, expected):
        if isinstance(active, Sequence):
            for val in active:
                self.plot_wrapper._toggle_fit_to_frame(val)
        else:
            self.plot_wrapper._toggle_fit_to_frame(active)

        assert self.plot_wrapper.plot.fit_to_frame_toggle.active == expected
