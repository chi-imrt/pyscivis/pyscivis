import numpy as np
import pytest

from pyscivis.visualizer.util.downsample import downsample_2d_mean_nojit
input_arr = np.array([
    [2, 4, 6],
    [8, 0, 12],
    [14, 16, 18]
], dtype=float)

input_arr_inf = np.array([
    [np.Infinity, np.Infinity],
    [np.Infinity, np.Infinity]
])


@pytest.mark.parametrize("arr, shape, expected", [
    (input_arr, input_arr.shape, input_arr),
    (input_arr, (1, 1),  np.array([[80/9]])),
    (input_arr_inf, (1, 1), np.array([[np.nan]]))
])
def test_downsample_2d_mean(arr, shape, expected):
    downsampled = downsample_2d_mean_nojit(arr, shape, np.nan)
    np.testing.assert_equal(downsampled, expected)


@pytest.mark.parametrize("arr, shape", [
    (input_arr, (3, 4)),
    (input_arr, (4, 3)),
    (input_arr, (4, 4)),
])
def test_downsample_2d_mean_with_invalid_shape(arr, shape):
    with pytest.raises(ValueError):
        downsample_2d_mean_nojit(arr, shape, np.nan)
