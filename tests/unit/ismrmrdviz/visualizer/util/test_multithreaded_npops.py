import pytest
import numpy as np
import numpy.testing as nptest
import pyscivis.visualizer.util.multithreaded_npops as mt
arr_0 = np.arange(100)
arr_1 = np.arange(100).view(dtype="complex64")
arr_2 = np.array([])


@pytest.mark.parametrize("arr, expected", [
    (arr_0, np.real(arr_0)),
    (arr_1, np.real(arr_1)),
    (arr_2, np.real(arr_2)),
])
def test_real(arr, expected):
    res = mt.real(arr)
    nptest.assert_array_equal(res, expected)
