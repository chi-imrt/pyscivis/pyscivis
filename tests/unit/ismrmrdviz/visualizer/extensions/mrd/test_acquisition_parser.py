from collections import namedtuple
from unittest.mock import Mock
from functools import reduce
import numpy as np
import pytest
from contextlib import ExitStack as NoException

from pyscivis.visualizer.dataclasses.config import ParserConfig
from pyscivis.visualizer.extensions.mrd.acquisition_parser import AcquisitionParser, chunk_ranges


def create_acq_header(avg, sli, con, pha, rep, set, seg, z, y):
    header = Mock()
    header.idx.kspace_encode_step_1 = y
    header.idx.kspace_encode_step_2 = z
    header.idx.average = avg
    header.idx.slice = sli
    header.idx.contrast = con
    header.idx.phase = pha
    header.idx.repetition = rep
    header.idx.set = set
    header.idx.segment = seg
    return header

def create_acqs(data, noise, **kwargs):
    DIMS = namedtuple("DIMS", ("avg", "slice", "contrast", "phase", "rep", "set", "segment", "z", "y"))
            # ("AVERAGE", "SLICE", "CONTRAST", "PHASE", "REPETITION", "SET", "SEGMENT", "Z", "Y", "X", "COIL")

    transpose_order = (0, 1, 2, 3, 4, 5, 6, 8, 9, 7, 10)
    data = np.transpose(data, transpose_order)  # we take [...]"COIL" val val "X"  as argument, but we want [...] "COIL" "X" for the parser
    dims = DIMS(*data.shape[:-2])  # X-Values and Coil are not necessary for specific acq-headers nor the acq-list

    amount_rows = reduce(lambda x, y: x*y, dims)
    acqs_list = np.empty(amount_rows, dtype=object)
    active_channels = data.shape[-2]  # coils
    acqs_header = {**dims._asdict(), **kwargs}
    header = create_acqs_header(**acqs_header)
    for idx, indices in enumerate(np.ndindex(dims)):
        acq_head = create_acq_header(*indices)

        acq_mock = Mock()
        acq_mock.active_channels = active_channels
        acq_mock.getHead = Mock(return_value=acq_head)
        acq_mock.data = data[indices]

        if noise[idx]:
            acq_mock.is_flag_set = Mock(return_value=True)
        else:
            acq_mock.is_flag_set = Mock(return_value=False)

        acqs_list[idx] = acq_mock
    acqs_data = acqs_list
    return acqs_data, header


def create_acqs_header(avg=None, slice=None, contrast=None, phase=None, rep=None,
                       set=None, segment=None, z=None, y=None, acc=(1, 1)):
    header = Mock()
    header.encoding = [Mock()]
    el = header.encoding[0].encodingLimits
    ep = header.encoding[0].parallelImaging

    if acc is not None:
        ep.accelerationFactor.kspace_encoding_step_1 = acc[0]
        ep.accelerationFactor.kspace_encoding_step_2 = acc[1]
    else:
        del ep.accelerationFactor.kspace_encoding_step_1
        del ep.accelerationFactor.kspace_encoding_step_2

    if y is not None:
        el.kspace_encoding_step_1.maximum = y-1
    else:
        del el.kspace_encoding_step_1.maximum

    if z is not None:
        el.kspace_encoding_step_2.maximum = z-1
    else:
        del el.kspace_encoding_step_2.maximum

    if avg is not None:
        el.average.maximum = avg-1
    else:
        del el.average.maximum

    if slice is not None:
        el.slice.maximum = slice-1
    else:
        del el.slice.maximum

    if contrast is not None:
        el.contrast.maximum = contrast-1
    else:
        del el.contrast.maximum

    if phase is not None:
        el.phase.maximum = phase-1
    else:
        del el.phase.maximum

    if rep is not None:
        el.repetition.maximum = rep-1
    else:
        del el.repetition.maximum

    if set is not None:
        el.set.maximum = set-1
    else:
        del el.set.maximum

    if segment is not None:
        el.segment.maximum = segment-1
    else:
        del el.segment.maximum

    return header


##################################################################################################
# "AVERAGE", "SLICE", "CONTRAST", "PHASE", "REPETITION", "SET", "SEGMENT", "Z", "Y", "COIL", "X" #
##################################################################################################
#                                       TEST BEGIN                                               #
##################################################################################################

DIM_NAMES = "AVERAGE", "SLICE", "CONTRAST", "PHASE", "REPETITION", "SET", "SEGMENT", "COIL", "Z", "Y", "X",

# fitting header and data, no noise
acq_data_0 = np.arange(2*2*2*3*4, dtype=np.complex64).reshape(2, 2, 2, 1, 1, 1, 1, 1, 1, 3, 4)
header_0 = {}
noise_arr_0 = np.zeros(2*2*2*3*4)
expected_0 = acq_data_0
error_0 = NoException()
parser_0 = ParserConfig()

# header sets field 'average' too high, no noise
acq_data_1 = np.arange(2*3*4, dtype=np.complex64).reshape(2, 1, 1, 1, 1, 1, 1, 1, 1, 3, 4)
header_1 = {"avg": 4}
noise_arr_1 = np.zeros(2*3*4)
expected_1 = np.append(acq_data_1, np.full(acq_data_1.shape, np.nan), axis=0)
error_1 = NoException()
parser_1 = ParserConfig()

# data is full of noise
acq_data_2 = np.arange(2, dtype=np.complex64).reshape(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2)
header_2 = {}
noise_arr_2 = np.ones(2)
expected_2 = None
error_2 = pytest.raises(ValueError)
parser_2 = ParserConfig()

# header does not set any optional fields
acq_data_3 = np.arange(2, dtype=np.complex64).reshape(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2)
header_3 = {"avg": None, "slice": None, "contrast": None, "phase": None,
            "rep": None, "set": None, "segment": None, "z": None, "acc": None}
noise_arr_3 = np.zeros(2)
expected_3 = acq_data_3
error_3 = NoException()
parser_3 = ParserConfig()

# first row is noise
acq_data_4 = np.arange(3, dtype=np.complex64).reshape(3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
header_4 = {}
noise_arr_4 = [1, 0, 0]
expected_4 = acq_data_4[:]
expected_4[0:] = None
error_4 = NoException()
parser_4 = ParserConfig()

# make sure chunk-size gets resetted if set too low
acq_data_5 = np.arange(3, dtype=np.complex64).reshape(3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
header_5 = {}
noise_arr_5 = np.zeros(3)
expected_5 = acq_data_5
error_5 = pytest.warns(UserWarning)
parser_5 = ParserConfig(chunk_size=1)

# test collapse-parallel
acq_data_6 = np.arange(16, dtype=np.complex64).reshape(1, 1, 1, 1, 1, 1, 1, 1, 4, 4, 1)
header_6 = {"acc": [2, 2]}
noise_arr_6 = np.zeros(16)
expected_6 = acq_data_6[..., 1::2, 1::2, ::]
error_6 = NoException()
parser_6 = ParserConfig(collapse_parallel=True)



@pytest.mark.parametrize("acq_data, header, noise, expected, exception, parser", [
    (acq_data_0, header_0, noise_arr_0, expected_0, error_0, parser_0),
    (acq_data_1, header_1, noise_arr_1, expected_1, error_1, parser_1),
    (acq_data_2, header_2, noise_arr_2, expected_2, error_2, parser_2),
    (acq_data_3, header_3, noise_arr_3, expected_3, error_3, parser_3),
    (acq_data_4, header_4, noise_arr_4, expected_4, error_4, parser_4),
    (acq_data_5, header_5, noise_arr_5, expected_5, error_5, parser_5),
    (acq_data_6, header_6, noise_arr_6, expected_6, error_6, parser_6),
])
def test_acquisition_parser(acq_data, noise, header, expected, exception, parser: ParserConfig):
    with exception:
        acquisition, header = create_acqs(acq_data, noise, **header)
        parsed_data = AcquisitionParser.parse_with_header(acquisition, header, parser, Mock())


        assert (parsed_data.dim_names, parsed_data.dim_units, parsed_data.dim_lengths)\
               == (DIM_NAMES, ("pixel", )*11, expected.shape)
        np.testing.assert_array_equal(parsed_data.data, expected)


def test_chunk_ranges():
    c_range = list(chunk_ranges(50, 9))
    assert c_range == [(0, 9), (9, 18), (18, 27), (27, 36), (36, 45), (45, 54)]