from contextlib import ExitStack as NoException
from unittest.mock import Mock

import pytest
from pyscivis.visualizer.dataclasses.config import ImageConfig, NotebookConfig, ParserConfig


class TestImageConfig:

    @pytest.mark.parametrize("value, expected", [
        (-1, pytest.raises(ValueError)),
        (1, NoException()),
        (0, NoException())
    ])
    # more general test because there's quite many attributes using this validator
    def test_validator_positive_int(self, value, expected):
        # non-integer values are caught by attrs (and their tests)
        with expected:
            ImageConfig.positive_int(Mock(), Mock(), value)

    @pytest.mark.parametrize("kwargs, expected", [
        (dict(max_height=1, min_height=2), pytest.raises(ValueError)),
        (dict(max_height=2, min_height=1), NoException()),
        (dict(max_width=1, min_width=2), pytest.raises(ValueError)),
        (dict(max_width=2, min_width=1), NoException()),
        (
        dict(initial_palette="pallete", palette_names=["palette"], palette_funcs=["turbo"]), pytest.raises(ValueError)),
        (
        dict(initial_palette="palette", palette_names=["palette"], palette_funcs=["turao"]), pytest.raises(ValueError)),
        (dict(initial_palette="palette", palette_names=["palette", "nocorrespondingfunc"], palette_funcs=["turbo"]),
         pytest.raises(ValueError)),
        (dict(initial_palette_size=2, palette_size=1), pytest.raises(ValueError)),
        (dict(initial_palette_size=1, palette_size=1), NoException()),
    ])
    def test_post_init(self, kwargs, expected):
        with expected:
            ImageConfig(**kwargs)


class TestNotebookConfig:
    @pytest.mark.parametrize("ports, expected", [
        ([-1, 1], pytest.raises(ValueError)),
        ([0, 1], pytest.raises(ValueError)),
        ([2, 1], pytest.raises(ValueError)),
        ([1, 1], NoException()),
        ([41, 411], NoException()),
    ])
    def test_valid_port_range(self, ports, expected):
        # non list(int) values are caught by attrs (and their tests)
        with expected:
            NotebookConfig(ports=ports)


class TestParserConfig:
    @pytest.mark.parametrize("val, expected", [
        (-1, pytest.raises(ValueError)),
        (0, pytest.raises(ValueError)),
        (1, NoException())
    ])
    def test_valid_chunk_size(self, val, expected):
        # non int values are caught by attrs (and their tests)
        with expected:
            ParserConfig(chunk_size=val)
