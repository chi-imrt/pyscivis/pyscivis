from contextlib import ExitStack as NoException
from unittest.mock import Mock

import pytest
from pyscivis.visualizer.dataclasses.state import *


class TestROIState:
    @pytest.mark.parametrize("value, expected", [
        ([], NoException()),
        ([1], NoException()),
        ([1, 1], pytest.raises(ValueError))
    ])
    def test_valid_length(self, value, expected):
        with expected:
            ROIState.valid_length(Mock(), Mock(), value)


json_state = """
    {"dimension":
         {"axes":
              {"x_axis": "X",
               "y_axis": "Y"},
          "dimensions": {"REPETITION": 1, "COIL": 1, "Y": 1, "X": 1}
          },
     "image": {"roi": {"x": [], "height": [], "y": [], "width": []},
               "y_range": {"end": 128.0, "bounds": [0.0, 128.0], "start": 0.0},
               "x_range": {"end": 256.0, "bounds": [0.0, 256.0], "start": 0.0}},
     "palette": {"length": 100, "window": [-0.00466897850856185, 0.0039265574887394905],
                 "name": "Greyscale", "cutoff": [-0.00466897850856185, 0.0039265574887394905]}, "profile": {"x": null, "y": null},
     "active_color_mapper": 0, "fit_to_frame": false, "value_kind": 0}
 """


@pytest.mark.parametrize("value, expected", [
    (json_state, True),
    ("{'some_val': '1'}", False),
])
def test_is_json_state(value, expected):
    assert is_json_state(value) is expected
