from unittest.mock import Mock

import pytest
from pyscivis.visualizer.plots.components.value_controls import ValueRadios, LABEL_FUNC_MAP


@pytest.fixture()
def value_radios():
    init_func_name = list(LABEL_FUNC_MAP.keys())[0]
    return ValueRadios(Mock(), init_func_name)


@pytest.mark.parametrize("new_index", [0, 1, 2, 3])
def test_set_active_handler(value_radios, new_index):
    value_radios.notify_parent = Mock()
    old_index = value_radios.rg.active
    value_radios.set_active_handler(new_index)

    if new_index == old_index:
        value_radios.notify_parent.assert_not_called()
    else:
        value_radios.notify_parent.assert_called_once()
