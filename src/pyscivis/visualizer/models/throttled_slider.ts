import {Slider} from "@bokehjs/models/widgets/slider"

export class ThrottledSlider extends Slider {
  static __module__ = "pyscivis.visualizer.models.throttled_slider"
}